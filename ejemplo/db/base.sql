-- Table: usuarios

-- DROP TABLE usuarios;

CREATE TABLE usuarios
(
  codigo integer NOT NULL,
  nombre character varying,
  ap character varying,
  am character varying,
  telefono integer,
  fnac date,
  codrol integer,
  CONSTRAINT codigo PRIMARY KEY (codigo ),
  CONSTRAINT codrol FOREIGN KEY (codrol)
      REFERENCES roles (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE usuarios
  OWNER TO postgres;



-- Table: roles

-- DROP TABLE roles;

CREATE TABLE roles
(
  id integer NOT NULL,
  nombre character varying,
  CONSTRAINT id PRIMARY KEY (id )
)
WITH (
  OIDS=FALSE
);
ALTER TABLE roles
  OWNER TO postgres;