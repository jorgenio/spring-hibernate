package model.dao;

import java.util.List;

import model.domain.Usuarios;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class usuariosDaoImpl  implements usuariosDao{
	SessionFactory sessionFactory;
    
    public SessionFactory getSessionFactory()    {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory)
    {
        this.sessionFactory = sessionFactory;
    }
	
	public void saveUsuarios(Usuarios usuarios){
    	Session session = this.sessionFactory.getCurrentSession();
        session.save(usuarios);
    }
	public void updateUsuarios(Usuarios usuarios){
    	Session session = this.sessionFactory.getCurrentSession();
        session.update(usuarios);
    }
	public void deleteUsuarios(Usuarios usuarios){
    	Session session = this.sessionFactory.getCurrentSession();
        session.delete(usuarios);
    }
	public void SaveOrUpdateUsuarios(Usuarios usuarios){
    	Session session = this.sessionFactory.getCurrentSession();
        session.saveOrUpdate(usuarios);
    }

	public Usuarios getUsuarios(int codigo) {
		Session session = this.sessionFactory.getCurrentSession();
    	return (Usuarios) session.get(Usuarios.class, codigo);
    }

	public Usuarios saveUsuarioscodigo(Usuarios usuarios) {
		Session session = this.sessionFactory.getCurrentSession();
        session.save(usuarios);
        return usuarios;
	}
	 public List getAllUsuarios() {		
			Session session = this.sessionFactory.getCurrentSession();
			return session.createQuery("from Usuarios").list();
	}
/**
 * metodos hibernate
 * session.delete(usuarios); es para borrar
 * session.save(usuarios); es para guardar
 * session.update(usuarios); es para modificar
 * session.saveOrUpdate(usuarios); es para guardar o modificar 
 * session.createQuery("from Usuarios").list(); es para 
 * introducir consultas en hibernate
 * session.get(Usuarios.class, codigo); es para recuperar el objeto
 * pero solo funciona con la clave primaria
 * **/
}
