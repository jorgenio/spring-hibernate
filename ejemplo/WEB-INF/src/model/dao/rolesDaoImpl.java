package model.dao;

import java.util.List;

import model.domain.Roles;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class rolesDaoImpl  implements rolesDao{
	SessionFactory sessionFactory;
    
    public SessionFactory getSessionFactory()    {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory)
    {
        this.sessionFactory = sessionFactory;
    }
	
	public void saveRoles(Roles roles){
    	Session session = this.sessionFactory.getCurrentSession();
        session.save(roles);
    }
	public void updateRoles(Roles roles){
    	Session session = this.sessionFactory.getCurrentSession();
        session.update(roles);
    }
	public void deleteRoles(Roles roles){
    	Session session = this.sessionFactory.getCurrentSession();
        session.delete(roles);
    }
	public void SaveOrUpdateRoles(Roles roles){
    	Session session = this.sessionFactory.getCurrentSession();
        session.saveOrUpdate(roles);
    }

	public Roles getRoles(int id) {
		Session session = this.sessionFactory.getCurrentSession();
    	return (Roles) session.get(Roles.class, id);
    }

	public Roles saveRolescodigo(Roles roles) {
		Session session = this.sessionFactory.getCurrentSession();
        session.save(roles);
        return roles;
	}
	 public List getAllRoles() {		
			Session session = this.sessionFactory.getCurrentSession();
			return session.createQuery("from Roles").list();
	}
/**
 * metodos hibernate
 * session.delete(roles); es para borrar
 * session.save(roles); es para guardar
 * session.update(roles); es para modificar
 * session.saveOrUpdate(roles); es para guardar o modificar 
 * session.createQuery("from Roles").list(); es para 
 * introducir consultas en hibernate
 * session.get(Roles.class, codigo); es para recuperar el objeto
 * pero solo funciona con la clave primaria
 * **/
}
