package model.dao;
import java.util.List;

import model.domain.Usuarios;

public interface usuariosDao {

	 void saveUsuarios(Usuarios usuarios); //guardar nuevo usuario
	 
	 void updateUsuarios(Usuarios usuarios); //modificar usuario
	 
	 void SaveOrUpdateUsuarios(Usuarios usuarios); //guardar omodifcar 
	 
	 void deleteUsuarios(Usuarios usuarios); //borrar 
	 	 
	 Usuarios saveUsuarioscodigo(Usuarios usuarios); // guarda y retorna el objeto guardado
	 
	 Usuarios getUsuarios(int codigo); //retorna un objeto que coincida con el parametro recibido en este caso el codigo 
	
	 public abstract List getAllUsuarios();
}
