package model.dao;
import java.util.List;

import model.domain.Roles;

public interface rolesDao {

	 void saveRoles(Roles roles); //guardar nuevo usuario
	 
	 void updateRoles(Roles roles); //modificar usuario
	 
	 void SaveOrUpdateRoles(Roles roles); //guardar omodifcar 
	 
	 void deleteRoles(Roles roles); //borrar 
	 	 
	 Roles saveRolescodigo(Roles roles); // guarda y retorna el objeto guardado
	 
	 Roles getRoles(int codigo); //retorna un objeto que coincida con el parametro recibido en este caso el codigo 
	
	 public abstract List getAllRoles();
}
