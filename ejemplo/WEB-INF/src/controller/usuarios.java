package controller;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import java.util.Date;
import java.text.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.dao.rolesDao;
import model.dao.usuariosDao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import java.io.*; 
import java.lang.*; 
import javax.servlet.*; 
import javax.servlet.http.*; 
import org.apache.velocity.Template;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.context.Context;
import org.apache.velocity.servlet.VelocityServlet;

import model.domain.Roles;
import model.domain.Usuarios;

public class usuarios extends MultiActionController implements InitializingBean{
	
	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub		
	}
	public usuarios() {
		// TODO Auto-generated constructor stub
	}
	private usuariosDao usuariosDao;
	private rolesDao rolesDao;
	public usuariosDao getUsuariosDao() {
		return usuariosDao;
	}
	public void setUsuariosDao(usuariosDao usuariosDao) {
		this.usuariosDao = usuariosDao;
	}
	
	public rolesDao getRolesDao() {
		return rolesDao;
	}
	public void setRolesDao(rolesDao rolesDao) {
		this.rolesDao = rolesDao;
	}
	public ModelAndView listausuarios(HttpServletRequest request,HttpServletResponse response) throws Exception,ServletException {
		HttpSession session=request.getSession(true);
		String codigosesionado=(String) session.getAttribute("codigo");
		if(codigosesionado==null){
			response.sendRedirect("index.html");	
		}
		Map map=new HashMap();
		List listausuarios=this.usuariosDao.getAllUsuarios();
		map.put("listaparalavista", listausuarios);
		return new ModelAndView("listausuarios", map);
    }
	public ModelAndView abmusuarios(HttpServletRequest request,HttpServletResponse response) throws Exception,ServletException {
		HttpSession session=request.getSession(true);
		String codigosesionado=(String) session.getAttribute("codigo");
		if(codigosesionado==null){
			response.sendRedirect("index.html");	
		}
		Map map=new HashMap();
		Usuarios usuario=new Usuarios();
		usuario.setNombre("");
		usuario.setAm("");
		usuario.setAp("");
		usuario.setTelefono(0);
		usuario.setCodigo(0);
		usuario.setFnac(new Date());
		map.put("usuariov", usuario);
		map.put("listaroles", this.rolesDao.getAllRoles());
		if(request.getParameter("guardar")!=null)
		{
			Usuarios usu=new Usuarios();
			usu.setNombre(request.getParameter("nombre"));
			usu.setAp(request.getParameter("ap"));
			usu.setAm(request.getParameter("am"));
			usu.setCodigo(Integer.parseInt(request.getParameter("codigo")));
			usu.setTelefono(Integer.parseInt(request.getParameter("telefono")));
			/**esto es para convertir la cadena a date 
			*  que estamos recuperando del formulario
			**/
			SimpleDateFormat fecha = new SimpleDateFormat("dd-MM-yyyy");
			fecha.setLenient(true);
			Date fnac = fecha.parse(request.getParameter("fnac"));
    		
			usu.setFnac(fnac);
			Roles roles=this.rolesDao.getRoles(Integer.parseInt(request.getParameter("rol")));
			usu.setRoles(roles);
 			this.usuariosDao.saveUsuarios(usu); // esto es para guardar 
 			//redireccionamos a listausuarios.html
 			response.sendRedirect("listausuarios.html");
		}
		if(request.getParameter("mod")!=null){
			int codigo=Integer.parseInt(request.getParameter("mod"));
			usuario=this.usuariosDao.getUsuarios(codigo);
			map.put("usuariov", usuario);
		}
		if(request.getParameter("cancelar")!=null){
			response.sendRedirect("listausuarios.html");
		}
		if(request.getParameter("modificar")!=null)
		{
			int codigo=Integer.parseInt(request.getParameter("codigo"));
			usuario=this.usuariosDao.getUsuarios(codigo);
			usuario.setNombre(request.getParameter("nombre"));
			usuario.setAp(request.getParameter("ap"));
			usuario.setAm(request.getParameter("am"));
			usuario.setTelefono(Integer.parseInt(request.getParameter("telefono")));
			/**esto es para convertir la cadena a date 
			*  que estamos recuperando del formulario
			**/
			SimpleDateFormat fecha = new SimpleDateFormat("dd-MM-yyyy");
			fecha.setLenient(true);
			Date fnac = fecha.parse(request.getParameter("fnac"));
    		usuario.setFnac(fnac);
			Roles roles=this.rolesDao.getRoles(Integer.parseInt(request.getParameter("rol")));
			usuario.setRoles(roles);
			this.usuariosDao.updateUsuarios(usuario);
			response.sendRedirect("listausuarios.html");
		}
		return new ModelAndView("abmusuarios", map);
    }
}