package controller;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import java.util.Date;
import java.text.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.dao.datosDao;
import model.domain.Datos;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import java.io.*; 
import java.lang.*; 
import javax.servlet.*; 
import javax.servlet.http.*; 
import org.apache.velocity.Template;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.context.Context;
import org.apache.velocity.servlet.VelocityServlet;

public class Principal extends MultiActionController implements InitializingBean{
	private datosDao datosDao;
	
	public datosDao getDatosDao() {
		return datosDao;
	}
	public void setDatosDao(datosDao datosDao) {
		this.datosDao = datosDao;
	}
	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub		
	}
	public Principal() {
		// TODO Auto-generated constructor stub
	}
	
	public ModelAndView index(HttpServletRequest request,HttpServletResponse response) throws Exception,ServletException {
		HttpSession session=request.getSession(true);
		
		Map map=new HashMap();
		String login=request.getParameter("usuario");
		String clave=request.getParameter("clave");
		Datos datos=this.datosDao.getLogearse(login, clave);
		if(datos!=null){
			session.setAttribute("codigo", Integer.toString(datos.getUsuarios().getCodigo()));
			System.out.print("codigo "+datos.getLogin());
			response.sendRedirect("main.html");
		}
		return new ModelAndView("index", map);
    }
	public ModelAndView main(HttpServletRequest request,HttpServletResponse response) throws Exception,ServletException {
		HttpSession session=request.getSession(true);
		Map map=new HashMap();
		String codigosesionado=(String) session.getAttribute("codigo");
		if(codigosesionado==null){
			response.sendRedirect("index.html");	
		}
		else{
			map.put("codigo", codigosesionado);
		}
		
		return new ModelAndView("main", map);
		
    }
	public ModelAndView salir(HttpServletRequest request,HttpServletResponse response) throws Exception,ServletException {
		HttpSession session=request.getSession(true);
		session.invalidate();
		return new ModelAndView("index");
    }
}